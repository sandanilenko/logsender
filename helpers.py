#!/app/Python-2.7.6/python
# coding=utf-8

import os
import sys
import datetime

import codecs
# import pysftp


class LogCollector:
    _starting_date = None
    _count_expire_days = 0
    _src_folders_paths_file_path = ''
    _src_folders_paths_list = []
    _logger = None

    def __init__(self, src_paths, starting_date, expire_days, logger):
        self._src_folders_paths_file_path = src_paths
        self._processed_paths = []
        self._starting_date = starting_date
        self._count_expire_days = expire_days
        self._logger = logger

    def _parse_paths(self):
        with open(self._src_folders_paths_file_path, 'r') as f:
            self._src_folders_paths_list = f.read().splitlines()

        if not self._src_folders_paths_list:
            self._logger.error(u'Файл {} пустой.'.format(self._src_folders_paths_file_path))
            raise IOError

        self._logger.info(u'Считано {} путей'.format(len(self._src_folders_paths_list)))

    def _collect(self):
        for path in self._src_folders_paths_list:
            self._logger.info(u'----- {}'.format(path))
            files = [f for f in os.listdir(path) if os.path.isfile(f)]

            for f in files:
                changed_at = datetime.datetime.fromtimestamp(int(os.stat(f).st_ctime)).date()
                self._logger.info('{} ---- {} {} {}'.format(f, changed_at.year, changed_at.month, changed_at.day))
                if (self._starting_date - changed_at).days <= self._count_expire_days:
                    self._processed_paths.append(path + f)

    def process(self):
        self._parse_paths()
        self._collect()

    def get_processed_paths(self):
        return self._processed_paths


class LogSender:
    """
    Класс для сбора и отправки логов на сторонний сервер

    _src_folders_paths_file: файл с абсолютными путями до папок с логами
    _src_folders_paths_list:
    """

    _trg_folder_path = ''
    _logger = None

    def __init__(self, src_folder_paths, trg_folder_path):
        pass

    def collect(self):
        pass

    def send(self):
        pass


# if __name__ == '__main__':
#     # считать путь к файлу из параметров вызова
#     pass
# def collect_new_logs():
#     pass
#     files = [f for f in os.listdir('.') if os.path.isfile(f)]
#
#     for f in files:
#         changed_at = datetime.datetime.fromtimestamp(int(os.stat(f).st_ctime))
#         print('{} ---- {} {} {}'.format(f, changed_at.year, changed_at.month, changed_at.day))
