#!/app/Python-2.7.6/python
# coding=utf-8

import logging
import sys
import getopt
import datetime

import os

from helpers import LogCollector, LogSender


if __name__ == '__main__':
    kwargs = ['log_dir_path=', 'log_file_mask=', 'src_folders_file_path=', 'count_expire_days=']
    options, argv = getopt.getopt(sys.argv[1:], '', kwargs)
    params = dict((key, value) for key, value in options)
    try:
        log_dir_path = params.get('--log_dir_path')
        log_file_mask = params.get('--log_file_mask')
        src_folders_file_path = params.get('--src_folders_file_path')
        count_expire_days = params.get('--count_expire_days')
    except KeyError:
        print(u'Неверно заданы параметны скрипта')
        raise IOError

    now = datetime.datetime.now()
    now_date_str = datetime.datetime.strftime(now, '%d.%m.%Y')

    logger = logging.getLogger()
    log_file_path = log_file_mask.format(
        log_dir_path=log_dir_path, now_date_str=now_date_str)
    if not os.path.exists(log_file_path):
        open(log_file_path, 'w+').close()
    logging.basicConfig(filename=log_file_path, level=logging.DEBUG, format='%(asctime)s %(message)s')

    collector = LogCollector(src_folders_file_path, now.date(), count_expire_days, logger)
    collector.process()

    print(collector.get_processed_paths())

    # if sources_paths_list:
    #     collector = LogCollector(sources_paths_list)
    #     collector.process()
    #
    #     sender = LogSender(collector.get_paths())
    # else:
